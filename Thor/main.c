#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
    int lightX; // Инициализация координаты Х "the light of power"
    int lightY; // Инициализация координаты Y "the light of power"
    int initialTX; // Инициализация начального положения Тора, координата Х
    int initialTY; // Инициализация начального положения Тора, координата Y
    scanf("%d%d%d%d", &lightX, &lightY, &initialTX, &initialTY);

    int thorX = initialTX; // Текущее положение Тора, координата Х
    int thorY = initialTY; // Текущее положение Тора, координата Y


    while (1) {
        int remainingTurns; // The remaining amount of turns Thor can move. Do not remove this line.
        scanf("%d", &remainingTurns);
        char *directX = ""; // Инициализация указателя Х
        char *directY = ""; // Инициализация указателя Y
        if (thorY > lightY) { // В случае, если Тор находится южнее чем "the light of power"
            directY = "N"; // Тор идет на север
            thorY--;
        } else if (thorY < lightY) { // В случае, если Тор находится севернее чем "the light of power"
            directY = "S"; // Тор идет на юг
            thorY++;
        }
        if (thorX > lightX) { // В случае, если Тор находится восточнее чем "the light of power"
            directX = "W"; // Тор идет на запад
            thorX--;
        } else if (thorX < lightX) { // В случае, если Тор находится западнее чем "the light of power"
            directX = "E"; // Тор идет на восток
            thorX++;
        }
        printf("%s%s\n", directY, directX); // Вывод искомого направления на экран
    }

    return 0;
}
